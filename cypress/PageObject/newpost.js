class NewPost{
    visit(){
        return cy.visit("https://next-realworld.vercel.app/editor/new")
    }

    getArticleTitle(){
        return cy.get(':nth-child(1) > .form-control')
    }

    getArticleDescription(){
        return cy.get(':nth-child(2) > .form-control')
    }

    getArticleContent(){
        return cy.get(':nth-child(3) > .form-control')
    }

    getArticleTag(){
        return cy.get(':nth-child(4) > .form-control')
    }

    getButton(){
        return cy.get('.btn')
    }

    enterArticleTitle(articleTitle){
        this.getArticleTitle().type(articleTitle)
    }

    enterArticleDescription(articleDescription){
        this.getArticleDescription().type(articleDescription)
    }

    enterArticleContent(articleContent){
        this.getArticleContent().type(articleContent)
    }

    enterArticleTags(){
        this.getArticleTag().type("lorem{enter}")
        this.getArticleTag().type("ipsum{enter}")
        this.getArticleTag().type("text{enter}")
    }

    verifyTitle(articleTitle){
        cy.contains(articleTitle)
    }

    verifyUrl(url){
        cy.url().should('include',url)
    }

    clickButton(){
        this.getButton().click()
    }

    verifyErrorMessage(){
        cy.get('ul.error-messages').should('be.visible')
    }




}

export default NewPost