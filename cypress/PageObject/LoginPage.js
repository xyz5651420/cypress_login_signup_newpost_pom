class LoginPage {
    visit(){
        cy.visit("https://next-realworld.vercel.app/user/login")
    } 
    getEmailField(){
        return cy.get(':nth-child(1) > .form-control')
    }
    getPasswordField(){
        return cy.get(':nth-child(2) > .form-control')
    }
    getButton(){
        return cy.get('.btn')
    }
    clickButton(){
        return cy.get('.btn').click()
    }
    urlVerify(url){
        return cy.url().should('eq', url)
    }
    errorMessageVerify(msg){
        cy.get('.error-messages > li').should('contain',msg)
    }
    pageElementVisible(){
        cy.get('.feed-toggle > .nav > :nth-child(1) > .nav-link').should('be.visible')
    }
    waitForElement(element){
        cy.contains(element);
    }
    enterEmail(email){
        this.getEmailField().type(email)
    }
    enterPassword(password){
        this.getPasswordField().type(password)
    }
}


export default LoginPage