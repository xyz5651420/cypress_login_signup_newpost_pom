class signupPage{
    visit(){
        cy.visit("https://next-realworld.vercel.app/user/register")
    }

    getUsername(){
        return cy.get(':nth-child(1) > .form-control')
    }

    getEmail(){
        return cy.get(':nth-child(2) > .form-control')
    }

    getPassword(){
        return cy.get(':nth-child(3) > .form-control')
    }

    getButton(){
       return cy.get('.btn')
    }

    enterUsername(username){
        this.getUsername().type(username)
    }

    enterEmail(email){
        this.getEmail().type(email)
    }

    enterPassword(password){
        this.getPassword().type(password)
    }

    clickButton(){
        this.getButton().click()
    }

    verifyPageElementInvisibility(){
        cy.get('.feed-toggle > .nav > :nth-child(1) > .nav-link').should('not.be.visible')
    }

    verifyPageElementVisibility(){
        cy.get('.feed-toggle > .nav > :nth-child(1) > .nav-link').should('be.visible')
    }

    verifyUrl(url){
        cy.url().should("eq", url)
    }

    verifyErrorMessage(msg){
    cy.get('.error-messages > li').should('contain',msg)
    }
}

export default signupPage