import signupPage from "../PageObject/SignUp"

describe("Signup Test",()=>{
    
    beforeEach(()=>{
        signUpPage.visit()
    })
    const signUpPage = new signupPage()

    it("Successful Sign UP",()=>{
        signUpPage.enterUsername("brightgreenegg")
        signUpPage.enterEmail("brightgreen@egg.com")
        signUpPage.enterPassword("yellowegg")
        signUpPage.clickButton()
        signUpPage.verifyPageElementVisibility()
    })

    it("Unsuccessful Sign UP",()=>{
        signUpPage.enterUsername("brightblueegg")
        signUpPage.enterEmail("brightgreen@egg.com")
        signUpPage.enterPassword("black")
        signUpPage.clickButton()
        signUpPage.verifyErrorMessage('email has already been taken')
    })

    it("Sign Up with existing account's username",()=>{
        signUpPage.enterUsername("brightgreenegg")
        signUpPage.enterEmail("bright@greenegg.com")
        signUpPage.enterPassword("red")
        signUpPage.clickButton()
        signUpPage.verifyErrorMessage("username has already been taken")
    })

    it("Sign Up with password under 8 characters",()=>{
        signUpPage.enterUsername("brightwhiteegg")
        signUpPage.enterEmail("brightwhite@egg.com")
        signUpPage.enterPassword("bright")
        signUpPage.clickButton()
        signUpPage.verifyPageElementInvisibility()
    })

    it("Sign Up with password over 14 characters",()=>{
        signUpPage.enterUsername("brightblackbluebulb")
        signUpPage.enterEmail("brightblackblue@bulb.com")
        signUpPage.enterPassword("brightblackbluebulbhurtseyes?")
        signUpPage.clickButton()
        signUpPage.verifyPageElementInvisibility()
    })

    it("Sign Up with email address without '@'",()=>{
        signUpPage.enterUsername("notdimbluebulb")
        signUpPage.enterEmail("notdimbluebulb.com")
        signUpPage.enterPassword("blue")
        signUpPage.clickButton()
        signUpPage.verifyUrl("https://next-realworld.vercel.app/user/register")
    })

    it("Sign Up with blank username",()=>{
        signUpPage.enterEmail("notbrightsky@bulb.com")
        signUpPage.enterPassword("brightsky")
        signUpPage.clickButton()
        signUpPage.verifyErrorMessage("username can't be blank")
    })

    it("Sign up with invalid email domain",()=>{
        signUpPage.enterUsername("notbrightbulb")
        signUpPage.enterEmail("notbright@bulb.com")
        signUpPage.enterPassword("invalidPassword")
        signUpPage.clickButton()
        signUpPage.verifyPageElementInvisibility()
    })

    it("Sign up with password over 8 characters and under 15 characters",()=>{
        signUpPage.enterUsername("notblackbright")
        signUpPage.enterEmail("notblackbright@bulb.com")
        signUpPage.enterPassword("notblack")
        signUpPage.clickButton()
        signUpPage.verifyPageElementVisibility()
    })

    it("Sign Up with special characters in username",()=>{
        signUpPage.enterUsername("notbrightyellow!")
        signUpPage.enterEmail("notbrightyellow@bulb.com")
        signUpPage.enterPassword("notbrightyellow")
        signUpPage.clickButton()
        signUpPage.verifyPageElementInvisibility()
    })
    })