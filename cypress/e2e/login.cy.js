import LoginPage from '../PageObject/LoginPage'

describe("Login with POM",()=>{
    beforeEach(()=>{
        loginPage.visit()
    })

    const loginPage = new LoginPage()

    it('Sign In with registered account',()=>{
        loginPage.enterEmail('vinegar@apple.com')
        loginPage.enterPassword('vinegar')
        loginPage.clickButton()
        loginPage.urlVerify("https://next-realworld.vercel.app/")
      })
      it('Sign In with unregistered account',()=>{
        loginPage.enterEmail('apple@soda.com')
        loginPage.enterPassword('soda')
        loginPage.clickButton()
        loginPage.errorMessageVerify('email or password is invalid')
      })
    
      it('Sign In with empty password field',()=>{
        loginPage.enterEmail('vinegar@apple.com')
        loginPage.clickButton()
        loginPage.errorMessageVerify("password can't be blank")
      })
    
      it('Sign In with empty email field',()=>{
        loginPage.enterPassword('12345')
        loginPage.clickButton()
        loginPage.errorMessageVerify("email can't be blank")
      })
    
      it('Sign In with incorrect password',()=>{
        loginPage.enterEmail('vinegar@apple.com')
        loginPage.enterPassword('apple')
        loginPage.clickButton()
        loginPage.errorMessageVerify("email or password is invalid")
      })
    
      it('Sign In with registered user account using Uppercase email input',()=>{
        loginPage.enterEmail('VINEGAR@APPLE.COM')
        loginPage.enterPassword('vinegar')
        loginPage.clickButton()
        loginPage.errorMessageVerify("email or password is invalid")
        
      })
    
      it('Sign In by clicking sign in button',()=>{
        loginPage.enterEmail('vinegar@apple.com')
        loginPage.enterPassword('vinegar')
        loginPage.clickButton()
        loginPage.pageElementVisible()
      })
    
      it('Sign in with registered user account using Uppercase password input',()=>{
        loginPage.enterEmail('vinegar@apple.com')
        loginPage.enterPassword('VINEGAR')
        loginPage.clickButton()
        loginPage.errorMessageVerify("email or password is invalid")
      })
    
      it('Sign in with empty email and password fields',()=>{
        cy.get('.btn').click()
        loginPage.errorMessageVerify("email can't be blank")
      })
    
      it("Sign in by pressing the enter key of the keyboard", () => {
        loginPage.enterEmail("vinegar@apple.com");
        loginPage.enterPassword("vinegar {enter}");
        loginPage.waitForElement("A place to share your knowledge") // wait to get the content after submitting the login form
        loginPage.urlVerify("https://next-realworld.vercel.app/");
      });
      
})