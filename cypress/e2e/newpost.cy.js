import NewPost from "../PageObject/newpost"

describe("New Post test",()=>{
    const newPostPage = new NewPost()

    beforeEach(()=>{
        newPostPage.visit()
    })

    const articleTitle = "Lorem Ipsum"
    const articleDescription = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
    const articleContent = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
  
    it("New Post Creation",()=>{
        newPostPage.enterArticleTitle(articleTitle)
        newPostPage.enterArticleDescription(articleDescription)
        newPostPage.enterArticleContent(articleContent)
        newPostPage.enterArticleTags()
        newPostPage.clickButton()
        newPostPage.verifyTitle(articleTitle)
        newPostPage.verifyUrl("/article")
    })

    it("New Post creation while only entering the title",()=>{
        newPostPage.enterArticleTitle(articleTitle)
        newPostPage.clickButton()
        newPostPage.verifyErrorMessage()
    })

    it("New Post creation without entering the title",()=>{
        newPostPage.enterArticleDescription(articleDescription)
        newPostPage.enterArticleContent(articleContent)
        newPostPage.enterArticleTags()
        newPostPage.clickButton()
        newPostPage.verifyErrorMessage()
    })

    it("New Post creation without entering the content",()=>{
        newPostPage.enterArticleTitle(articleTitle)
        newPostPage.enterArticleDescription(articleDescription)
        newPostPage.enterArticleTags()
        newPostPage.clickButton()
        newPostPage.verifyErrorMessage()
    })

    it("New Post creation without entering the tags",()=>{
        newPostPage.enterArticleTitle(articleTitle)
        newPostPage.enterArticleDescription(articleDescription)
        newPostPage.enterArticleContent(articleContent)
        newPostPage.clickButton()
        newPostPage.verifyErrorMessage()
    })

    it("New post creation with all input fields blank",()=>{

        newPostPage.clickButton()
        newPostPage.verifyErrorMessage()
    })
})